//===- StaticTDGIdent.cpp -- Strip parts of Debug Info --------------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//

#include "llvm/Transforms/DynamicVariant.h"
#include "llvm/ADT/DepthFirstIterator.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/DebugInfo.h"
#include "llvm/IR/Dominators.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/InitializePasses.h"
#include "llvm/Pass.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Transforms/Utils/ModuleUtils.h"
#include "llvm/Transforms/Utils/ValueMapper.h"
using namespace llvm;

#define DEBUG_TYPE "dynamic-variant"

namespace {

struct variantInfo{
  Instruction *inst;
  SmallVector<Function *, 8> functions;
  GlobalVariable *functions_names;
  StringRef original_name;
  Instruction *variant_call;
};

void createMallocWrapping(Module &M) {
  FunctionType *RealMallocType =
      FunctionType::get(Type::getInt8PtrTy(M.getContext()),
                        {Type::getInt64Ty(M.getContext())}, false);
  Function *RealMallocFunc = Function::Create(
      RealMallocType, Function::ExternalWeakLinkage, "__real_malloc", M);

  // Define the __wrap_malloc function
  Function *WrapMallocFunc = Function::Create(
      RealMallocType, Function::ExternalWeakLinkage, "__wrap_malloc", M);

  // Define the __kmpc_malloc_cpu_mem function
  FunctionType *KmpcMallocType = FunctionType::get(
      Type::getInt8PtrTy(M.getContext()),
      {Type::getInt8PtrTy(M.getContext()), Type::getInt64Ty(M.getContext())},
      false);
  Function *KmpcMallocFunc = Function::Create(
      KmpcMallocType, Function::ExternalLinkage, "__kmpc_malloc_cpu_mem", M);

  // Create entry basic block for __wrap_malloc
  BasicBlock *MallocEntryBB =
      BasicBlock::Create(M.getContext(), "entry", WrapMallocFunc);
  IRBuilder<> IRB(MallocEntryBB);

  // Get argument of __wrap_malloc
  Argument *MallocArg = &*(WrapMallocFunc->arg_begin());

  // Call __real_malloc with the same argument
  Value *RealMallocCall = IRB.CreateCall(RealMallocFunc, MallocArg);

  // Call __kmpc_malloc_cpu_mem with the pointer received from __real_malloc and
  // the size argument of __wrap_malloc
  Value *KmpcMallocArgs[] = {RealMallocCall, MallocArg};
  IRB.CreateCall(KmpcMallocFunc, KmpcMallocArgs);

  // Return the result of __real_malloc
  IRB.CreateRet(RealMallocCall);
}

void createFreeWrapping(Module &M) {
  FunctionType *RealFreeType =
      FunctionType::get(Type::getVoidTy(M.getContext()),
                        {Type::getInt8PtrTy(M.getContext())}, false);
  Function *RealFreeFunc = Function::Create(
      RealFreeType, Function::ExternalWeakLinkage, "__real_free", M);

  Function *WrapFreeFunc = Function::Create(
      RealFreeType, Function::ExternalWeakLinkage, "__wrap_free", M);

  Function *KmpcFreeFunc = Function::Create(
      RealFreeType, Function::ExternalLinkage, "__kmpc_free_cpu_mem", M);

  BasicBlock *FreeEntryBB =
      BasicBlock::Create(M.getContext(), "entry", WrapFreeFunc);
  IRBuilder<> IRB(FreeEntryBB);

  IRB.SetInsertPoint(FreeEntryBB);

  // Get argument of __wrap_free
  Argument *FreeArg = &*(WrapFreeFunc->arg_begin());

  // Call __real_free with the same argument
  IRB.CreateCall(RealFreeFunc, FreeArg);
  IRB.CreateCall(KmpcFreeFunc, FreeArg);
  IRB.CreateRetVoid();
}

void createNewWrapping(Module &M) {

  // Define the __wrap_malloc function
  Function *NewFunc = M.getFunction("_Znwm");

  if (NewFunc) {
    NewFunc->setLinkage(Function::InternalLinkage);
    Function *KmpcMallocFunc = M.getFunction("__kmpc_malloc_cpu_mem");
    if (!KmpcMallocFunc) {
      FunctionType *KmpcMallocType =
          FunctionType::get(Type::getInt8PtrTy(M.getContext()),
                            {Type::getInt8PtrTy(M.getContext()),
                             Type::getInt64Ty(M.getContext())},
                            false);
      KmpcMallocFunc =
          Function::Create(KmpcMallocType, Function::ExternalLinkage,
                           "__kmpc_malloc_cpu_mem", M);
    }
    // Create entry basic block
    BasicBlock *entryBB = BasicBlock::Create(M.getContext(), "entry", NewFunc);
    IRBuilder<> builder(entryBB);

    // Get function argument
    Argument *argSize = &*NewFunc->arg_begin();

    // Call to malloc
    Function *RealMallocFunc = M.getFunction("malloc");
    if (!RealMallocFunc) {
      FunctionType *RealMallocType =
          FunctionType::get(Type::getInt8PtrTy(M.getContext()),
                            {Type::getInt64Ty(M.getContext())}, false);
      RealMallocFunc = Function::Create(
          RealMallocType, Function::ExternalWeakLinkage, "malloc", M);
    }
    Value *mallocCall = builder.CreateCall(RealMallocFunc, {argSize});
    Value *KmpcMallocArgs[] = {mallocCall, argSize};
    builder.CreateCall(KmpcMallocFunc, KmpcMallocArgs);
    // Return malloc result
    builder.CreateRet(mallocCall);
  }
}

void createDeleteWrapping(Module &M) {

  // Define the __wrap_malloc function
  Function *NewFunc = M.getFunction("_ZdlPv");

  if (NewFunc) {
    NewFunc->setLinkage(Function::InternalLinkage);
    Function *KmpcFreeFunc = M.getFunction("__kmpc_free_cpu_mem");
    if (!KmpcFreeFunc) {
      FunctionType *KmpcFreeType =
          FunctionType::get(Type::getVoidTy(M.getContext()),
                            {Type::getInt8PtrTy(M.getContext())},
                            false);
      KmpcFreeFunc =
          Function::Create(KmpcFreeType, Function::ExternalLinkage,
                           "__kmpc_free_cpu_mem", M);
    }
    // Create entry basic block
    BasicBlock *entryBB = BasicBlock::Create(M.getContext(), "entry", NewFunc);
    IRBuilder<> builder(entryBB);

    // Get function argument
    Argument *argSize = &*NewFunc->arg_begin();

    // Call to free
    Function *RealFreeFunc = M.getFunction("free");
    if (!RealFreeFunc) {
      FunctionType *RealFreeType =
          FunctionType::get(Type::getVoidTy(M.getContext()),
                            {Type::getInt8PtrTy(M.getContext())}, false);
      RealFreeFunc = Function::Create(
          RealFreeType, Function::ExternalWeakLinkage, "free", M);
    }
    builder.CreateCall(RealFreeFunc, {argSize});
    Value *KmpcFreeArgs[] = {argSize};
    builder.CreateCall(KmpcFreeFunc, KmpcFreeArgs);
    // Return malloc result
    builder.CreateRetVoid();
  }
}

bool isUnreachable(BasicBlock *BB) {
    return !isa<ReturnInst>(BB->getTerminator());
}

struct DynamicVariant {
  DynamicVariant() {}

  bool runOnModule(Module &M) {
    if (M.empty())
      return false;

    // Check if we need to add instrumentation
    bool InsertInstrumentation = false;
    if (M.getModuleFlag("dynamic-variant-warmup"))
      InsertInstrumentation = true;
    bool VariantFound = false;

    Type *returnType = Type::getInt32Ty(M.getContext());
    Type *CharPtrTy = Type::getInt8PtrTy(M.getContext());
    PointerType *CharPtrPtrTy = PointerType::get(CharPtrTy, 0);
    std::vector<Type *> argTypes = {
        CharPtrPtrTy, Type::getInt32PtrTy(M.getContext()), Type::getInt32Ty(M.getContext()),
        Type::getInt32PtrTy(M.getContext())};

    FunctionType *functionType = FunctionType::get(returnType, argTypes, false);
    auto DynVariantF =
        M.getOrInsertFunction("__kmpc_dynamic_variant", functionType);

    FunctionType *StartVariantFunctionType =
        FunctionType::get(Type::getVoidTy(M.getContext()), false);
    FunctionType *EndVariantFunctionType =
        FunctionType::get(Type::getVoidTy(M.getContext()), {Type::getInt8PtrTy(M.getContext())}, false);
    FunctionType *InitInstrumentationFunctionType =
        FunctionType::get(Type::getVoidTy(M.getContext()), false);

    FunctionType *GetNumExecutionsFunctionType =
        FunctionType::get(Type::getInt32Ty(M.getContext()), false);

    auto StartVariantInstrumentationFunction = M.getOrInsertFunction(
        "__kmpc_instrument_variant_start", StartVariantFunctionType);
    auto EndVariantInstrumentationFunction = M.getOrInsertFunction(
        "__kmpc_instrument_variant_end", EndVariantFunctionType);

    auto InitInstrumentationFunction = M.getOrInsertFunction(
        "__kmpc_init_instrumentation", InitInstrumentationFunctionType);
    auto FinishInstrumentationFunction = M.getOrInsertFunction(
        "__kmpc_finish_instrumentation", StartVariantFunctionType);

    auto GetNumExecutionsFunction = M.getOrInsertFunction(
        "__kmpc_get_num_executions", GetNumExecutionsFunctionType);

    for (auto &F : M) {
      // Nothing to do for declarations.
      if (F.isDeclaration() || F.empty())
        continue;

      // Vector to store pairs of dynamic variant calls and function vector
      SmallVector<variantInfo, 8> VariantCallsFound;

      // Vectors to traverse BB
      SmallVector<BasicBlock *, 8> Worklist;
      SmallPtrSet<BasicBlock *, 8> Visited;
      Worklist.push_back(&F.getEntryBlock());
      Visited.insert(&F.getEntryBlock());

      // Iterate over BB
      while (!Worklist.empty()) {
        auto WIt = Worklist.begin();
        BasicBlock *BB = *WIt;
        Worklist.erase(WIt);

        for (Instruction &I : *BB) {
          if (CallBase *FuncCall = dyn_cast<CallBase>(&I))
            if (FuncCall->getCalledFunction() &&
                FuncCall->getCalledFunction()->hasFnAttribute("variant")) {

              IRBuilder<> IRB(&F.getEntryBlock());
              VariantFound = true;
              // Get global structures names
              StringRef MainVariantName = FuncCall->getCalledFunction()
                                              ->getFnAttribute("variant")
                                              .getValueAsString();

              std::string VariantsFunctions = MainVariantName.str() + "_variants";
              std::string TraitsName = MainVariantName.str() + "_traits";
              std::string VariantsFunctionsName = MainVariantName.str() + "_variants_names";

              // Vector to store variants functions
              SmallVector<Function *, 8> VariantFuncions;

              // Obtain and store all variants functions
              GlobalVariable *FunctionsArray =
                  M.getGlobalVariable(VariantsFunctions, true);
              ConstantArray *FunctionsConstantArray =
                  dyn_cast<ConstantArray>(FunctionsArray->getOperand(0));
              int numVariants = FunctionsConstantArray->getNumOperands();

              for (int i = 0; i < numVariants; i++) {
                VariantFuncions.push_back(
                    dyn_cast<Function>(FunctionsConstantArray->getOperand(i)));
              }

              // Build dynamic variant runtime call and store pair
              IRB.SetInsertPoint(&I);

              // Parse user conditions, look for annotations
              SmallVector<Value *, 2> UserConditions;
              Instruction *Start = I.getPrevNode();
              while (Start) {
                if (dyn_cast<CallInst>(Start) &&
                    !dyn_cast<IntrinsicInst>(Start))
                  break;
                else if (Start->getMetadata("annotation")) {
                  UserConditions.insert(UserConditions.begin(), Start);
                }
                Start = Start->getPrevNode();
              }

              Value *UserConditionsArray = ConstantPointerNull::get(
                  PointerType::getInt32PtrTy(M.getContext()));
              ArrayType *UserConditionsArrayType =
                  ArrayType::get(IRB.getInt32Ty(), UserConditions.size());
              // Store user conditions
              if (UserConditions.size()) {
                UserConditionsArray = IRB.CreateAlloca(UserConditionsArrayType);
                int Index = 0;
                for (Value *Condition : UserConditions) {
                   Value *GEP = IRB.CreateInBoundsGEP(
                       UserConditionsArrayType, UserConditionsArray,
                       {IRB.getInt32(0), IRB.getInt32(Index)});
                   if (dyn_cast<AllocaInst>(Condition))
                     Condition = IRB.CreateLoad(IRB.getInt1Ty(), Condition);
                   Value *ConditionBitcast =
                       IRB.CreateIntCast(Condition, IRB.getInt32Ty(), false);
                   IRB.CreateStore(ConditionBitcast, GEP);
                   Index++;
                }
              }

              GlobalVariable *TraitsArray =
                  M.getGlobalVariable(TraitsName, true);
              Value *TraitsBitCast = IRB.CreateBitCast(
                  TraitsArray, Type::getInt32PtrTy(M.getContext()));

              GlobalVariable *VariantFunctionsNamesArray =
                  M.getGlobalVariable(VariantsFunctionsName, true);
              Value *VariantsNamesBitCast =
                  IRB.CreateBitCast(VariantFunctionsNamesArray, CharPtrPtrTy);

              Value *UserConditionsArrayBitcast =
                  IRB.CreateInBoundsGEP(UserConditionsArrayType,
                                        UserConditionsArray, IRB.getInt32(0));

              Instruction *ChoosedVariant =
                  dyn_cast<Instruction>(IRB.CreateCall(
                      DynVariantF,
                      {VariantsNamesBitCast, TraitsBitCast,
                       IRB.getInt32(numVariants), UserConditionsArrayBitcast}));

              if (InsertInstrumentation)
                IRB.CreateCall(StartVariantInstrumentationFunction);

              VariantCallsFound.push_back(
                  {ChoosedVariant, VariantFuncions, VariantFunctionsNamesArray, FuncCall->getCalledFunction()->getName(), &I});
            }
        }

        for (auto It = succ_begin(BB); It != succ_end(BB); ++It) {
          if (!Visited.count(*It)) {
            Worklist.push_back(*It);
            Visited.insert(*It);
          }
        }
      }

      // The transformation alterates the BBs hierarchy, so we can only
      // transform after the BB iteration of the Function is done. All the
      // transformation needed are now stored in pairs inside VariantCallsFoung
      for (auto VariantPair : VariantCallsFound) {

        auto *SwitchStart = VariantPair.inst;
        auto Functions = VariantPair.functions;
        Constant *StrArray = VariantPair.functions_names->getInitializer();

        // First, we create a switch just after the dynamic variant call
        // And after instrumentation
        auto *VariantCall = VariantPair.variant_call;
        IRBuilder<> IRB(VariantCall);
        auto *VariantSwitch = IRB.CreateSwitch(SwitchStart, SwitchStart->getParent(), Functions.size());

        // Second, we split the BB after the switch. This is the variant_end
        BasicBlock *VariantEnd =
            (SwitchStart->getParent())
                ->splitBasicBlock(VariantSwitch->getNextNode(), "variant_end");

        // Then, for each variant we create a separate BB
        for (int i = 0; i < (int)Functions.size(); i++) {
          BasicBlock *SwitchCase =
              BasicBlock::Create(M.getContext(), "variant", &F, VariantEnd);
          IRB.SetInsertPoint(SwitchCase);

          // Clone the variant call and replace the called function. This works
          // as all variants must maintain the same call arguments
          auto *NewVariantCall = VariantCall->clone();
          CallInst *Cast = dyn_cast<CallInst>(NewVariantCall);
          Cast->setCalledFunction(Functions[i]);
          IRB.Insert(NewVariantCall);

          if (InsertInstrumentation)
            IRB.CreateCall(EndVariantInstrumentationFunction, {StrArray->getAggregateElement(i)});

          // Create terminator
          IRB.CreateBr(VariantEnd);

          // Add new case to the switch
          VariantSwitch->addCase(IRB.getInt32(i), SwitchCase);
        }

        // Add the default switch case, that corresponds to the original
        // compiler variant.
        BasicBlock *DefaultCase = BasicBlock::Create(
            M.getContext(), "default_variant", &F, VariantEnd);
        IRB.SetInsertPoint(DefaultCase);
        //Last function name was the original call
        auto *NewVariantCall = VariantCall->clone();
        IRB.Insert(NewVariantCall);
        if (InsertInstrumentation) {
          llvm::Constant *OriginalVariantNameStr =
              llvm::ConstantDataArray::getString(F.getParent()->getContext(),
                                                 VariantPair.original_name);

          llvm::GlobalVariable *OriginalVariantNameGV =
              new llvm::GlobalVariable(
                  *F.getParent(), OriginalVariantNameStr->getType(), true,
                  llvm::GlobalValue::PrivateLinkage, OriginalVariantNameStr);

          IRB.CreateCall(
              EndVariantInstrumentationFunction, {OriginalVariantNameGV});
        }
        IRB.CreateBr(VariantEnd);

        // Replace switch default dest, remove unnecesary BB terminator added by
        // the split, and remove the original compiler variant call
        VariantSwitch->setDefaultDest(DefaultCase);
        VariantSwitch->getNextNode()->eraseFromParent();
        VariantCall->eraseFromParent();
      }
    }

    // Remove all function variant attributes. This will prevent the compiler
    // from generating the variants twice in case this optimizacion runs again
    for (auto &F : M) {
      F.removeFnAttr("variant");
      if (!strcmp(F.getName().str().c_str(), "main") && InsertInstrumentation) {
        IRBuilder<> IRB(&(*F.getEntryBlock().getFirstInsertionPt()));

        IRB.CreateCall(InitInstrumentationFunction);
        BasicBlock *LastBlock = &F.back();
        // The last basic block may be unreacheable
        if (isUnreachable(LastBlock)) {
          LastBlock = LastBlock->getPrevNode();
        }
        Instruction *Terminator = LastBlock->getTerminator();

        IRB.SetInsertPoint(Terminator);
        IRB.SetInsertPoint(IRB.CreateCall(FinishInstrumentationFunction));

        // Check if the global variable has been initialized
        GlobalVariable *ExecutionsVar =
            F.getParent()->getGlobalVariable("main_executions");
        Type *Int32Ty = Type::getInt32Ty(F.getParent()->getContext());
        if (!ExecutionsVar) {
          // Create a global variable to track the number of executions
          ExecutionsVar = new GlobalVariable(
              *F.getParent(), Int32Ty, false, GlobalValue::InternalLinkage,
              ConstantInt::get(Int32Ty, 0), "main_executions");
        }
        // Increment the execution count
        Instruction *ExecutionCount = IRB.CreateLoad(Int32Ty, ExecutionsVar);
        Value *UpdatedCount =
            IRB.CreateAdd(ExecutionCount, ConstantInt::get(Int32Ty, 1));
        IRB.CreateStore(UpdatedCount, ExecutionsVar);

        Value *MaxNum = IRB.CreateCall(GetNumExecutionsFunction);
        // Check if this is the second execution
        Value *IsSecondExecution = IRB.CreateICmpEQ(UpdatedCount, MaxNum);

        BasicBlock *EndBB =
            LastBlock->splitBasicBlock(Terminator->getPrevNode(), "end");
        BasicBlock *NextBB = BasicBlock::Create(F.getParent()->getContext(),
                                                "run_main", &F, EndBB);

        // Create a branch to either run main or terminate
        IRB.SetInsertPoint(ExecutionCount->getParent());
        Instruction *NewTerminator =
            IRB.CreateCondBr(IsSecondExecution, EndBB, NextBB);
        NewTerminator->getPrevNode()->eraseFromParent();
        IRB.SetInsertPoint(NextBB);

        Value *MainFuncPtr =
            IRB.CreatePointerCast(&F, F.getFunctionType()->getPointerTo());

        // Create a vector to store the arguments of the call
        std::vector<Value *> CallArgs;

        // Iterate over the arguments of main and add them to the call arguments
        for (auto &Arg : F.args()) {
          CallArgs.push_back(&Arg);
        }

        IRB.CreateCall(F.getFunctionType(), MainFuncPtr, CallArgs);

        // Branch to the end
        IRB.CreateBr(EndBB);
        IRB.SetInsertPoint(EndBB);
      }
    }

    if (InsertInstrumentation) {
      //createMallocWrapping(M);
      //createFreeWrapping(M);
      //createNewWrapping(M);
      //createDeleteWrapping(M);
    }

    return VariantFound;
  }
};

struct DynamicVariantLegacyPass : public ModulePass {
  /// Pass identification, replacement for typeid
  static char ID;
  DynamicVariantLegacyPass() : ModulePass(ID) {
    initializeDynamicVariantLegacyPassPass(*PassRegistry::getPassRegistry());
  }

  bool runOnModule(Module &M) override {
    if (skipModule(M))
      return false;

    // See below for the reason of this.
    return DynamicVariant().runOnModule(M);
  }

  StringRef getPassName() const override {
    return "Replace variants with a dynamic runtime decision";
  }

  void getAnalysisUsage(AnalysisUsage &AU) const override {
  }
};

} // namespace

char DynamicVariantLegacyPass::ID = 0;

ModulePass *llvm::createDynamicVariantLegacyPass() {
  return new DynamicVariantLegacyPass();
}

void LLVMDynamicVariantPass(LLVMPassManagerRef PM) {
  unwrap(PM)->add(createDynamicVariantLegacyPass());
}

INITIALIZE_PASS_BEGIN(DynamicVariantLegacyPass, "dynamic-variant",
                      "Replace variants with a dynamic runtime decision", false, false)
INITIALIZE_PASS_END(DynamicVariantLegacyPass, "dynamic-variant",
                    "Replace variants with a dynamic runtime decision", false, false)

// New pass manager.
PreservedAnalyses DynamicVariantPass::run(Module &M,
                                          ModuleAnalysisManager &AM) {
  // This is an adapter that will be used to get the analysis data of a given
  // function. This is done this way so we can reuse the code for the new and
  // legacy pass manager (they used different functions to get this
  // information).
  if (!DynamicVariant().runOnModule(M))
    return PreservedAnalyses::all();

  // FIXME: we can be more precise here.
  return PreservedAnalyses::none();
}
